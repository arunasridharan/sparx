:orphan:

Quickstart
===========

This quickstart is here to show some simple ways to get started created
and manipulating Blaze Symbols. To run these examples, import blaze
as follows.

.. code-block:: python

    >>> from sparx.preprocess import *

is_categorical
~~~~~~~~~~~~~~~~~~~~~~

Check if the given pandas series is an categorical variable ```True```

.. code-block:: python

    >>> is_categorical(data[col])
    >>> True


is_date
~~~~~~~~~~~~~~~~~~~

Return ```True``` if the given pandas series is an date type

.. code-block:: python

    >>> is_date(data[col])
    >>> True


count_missing
~~~~~~~~~~~~~~~~~~~

Return the count of missing values in the given pandas.core.series

.. code-block:: python

    >>> count_missing(df['col_name'])
    >>> 0

types
~~~~~~~~~~~~~~~~~~~

Returns the column names in groups for the given DataFrame

.. code-block:: python

    >>> types(df)
    >>> {'dates': ['D'],
    ...  'groups': ['C', 'D'],
    ...  'keywords': ['C'],
    ...  'numbers': ['A', 'B']}


has_keyword
~~~~~~~~~~~~~~~~~~~

Returns ``True`` if any of the first 1000 non-null values in a string``series`` are strings that have more than ``thresh`` =2 separators (space, by default) in them

.. code-block:: python

    >>> has_keywords(series)
    >>> False
    >>> has_keywords(series, thresh=1)
    >>> True   

groupmeans
~~~~~~~~~~~~~~~~~~~

Yields the significant differences in average between every pair of groups and numbers.

.. code-block:: python

    >>> has_keywords(series)
    >>> False
    >>> has_keywords(series, thresh=1)
    >>> True  


describe
~~~~~~~~~~~~~~~~~~~

Return the basic description of an column in a pandas dataframe check if the column is an interger or float type

.. code-block:: python

    >>> describe(dataframe, 'Amount')
    >>> {'min': 0, 'max': 100, 'mean': 50, 'median': 49 }
 


geocode
~~~~~~~~~~~~~~~~~~~

Return ```Dict``` which consist of address, latitude, longitude of the given address

.. code-block:: python

    >>> geocode("172, 5th Avenue, Flatiron, Manhattan")
    >>> {'latitude': 40.74111015, 'adress': u'172, 5th Avenue, Flatiron,
             Manhattan, Manhattan Community Board 5, New York County, NYC,
             New York, 10010, United States of America',
            'longitude': -73.9903105}